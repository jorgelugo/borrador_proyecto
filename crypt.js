const bcrypt  = require('bcrypt');

//Returns encrypted String
function hash(data){
  console.log("Hashing data");
  return bcrypt.hashSync(data,10);
}

function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
 console.log("Checking password");

 return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
