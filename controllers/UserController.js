const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujllr10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getUserV1(req,res){
  console.log("GET /apptechu/v1/users");
  //res.sendFile('usuarios.json', {root: __dirname}); //__dirname nos indica el punto donde se ejecuta este js
  var respuesta = {};
  var users = require('../usuarios.json'); //tengo que ponerle el punto porque si no va directamente a node_modules a buscar el archivo

  if (req.query.$count && req.query.$count == 'true'){
    respuesta.count = users.length;
  }

  var top = (req.query.$top) ? req.query.$top : users.length;

  respuesta.users = users.slice(0,top);
  res.send(respuesta);
}

function getUserV2(req,res){
  console.log("GET /apptechu/v2/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  console.log("user?" + mLabAPIKey);
  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body : {"msg" : "Error obteniendo usuarios"}

      res.send(response);
    }
  );
}

function getUserByIdV1(req,res){
  console.log("GET /apptechu/v1/users/:id");

  var users = require('../usuarios.json');
  var user = {}
  for (index in users){
    if (users[index].id == req.params.id){
        console.log(index);
        user = users[index];
        break;
    }
  }

  res.send(user);
}

function getUserByIdV2(req,res){
  console.log("GET /apptechu/v2/users/:id");

  var q = "q={id:" + req.params.id + "}&";


  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  httpClient.get("user?" + q + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = {"msg" : "Error obteniendo el usuario"};
        res.status(500);
      }else{
        if (body.length > 0){
          var response = body[0];
        }else{
          var response = {"msg" : "Usuario no encontrado"};
          res.status(404);
        }
      }
      res.send(response);
    });
}

function setUserV1(req,res){
    console.log("POST /apptechu/v1/users");

    console.log(req.body);

    var users = require('../usuarios.json');
    var idNew = 0;
    users.forEach(function(item){
      idNew =  (item.id > idNew) ? item.id : idNew;
    })
    var newUser = {
      "id" : idNew + 1,
      "first_name" :req.body.first_name ,
      "last_name" : req.body.last_name,
      "email" : req.body.email
    };

    users.push(newUser);

    var serialUsers = JSON.stringify(users);
    io.writeUserDataFile(users);

    res.send({"msg" : "Usuario añadido con éxito"});
}

function setUserV2(req,res){
    console.log("POST /apptechu/v2/users");

    var newUser = {
      "id" : req.body.id,
      "first_name" :req.body.first_name ,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : crypt.hash(req.body.password)
    };

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Client created");

    httpClient.post("user?" + mLabAPIKey, newUser,
      function(err, resMLab, body){
        res.status(201).send({"msg" : "Usuario guardado"});
      }
    );
}

function deleteUserByIdV1(req,res){
    console.log("DELETE /apptechu/v1/users/:id");

    var users = require('../usuarios.json');
    var indice = -1;

    //For normal
    for (var i = 0; i < users.length; i++){
      if (users[i].id == req.params.id){
          indice = i;
          break;
        }
    }
    console.log("For normal: " + indice);

    //for in
    for (index in users){
      if (users[index].id == req.params.id){
          indice = index;
          break;
      }
    }
    console.log("For in: " + indice);

    //for of
    for (var [index, user] of users.entries()){
      if (user.id == req.params.id){
        indice = index;
        break;
      }
    }
    console.log("For of (entries): " + indice);

    //for of
    var indAux=0;
    for (var user of users){
      if (user.id == req.params.id){
        indice = indAux;
        break;
      }
      indAux++;
    }
    console.log("For of: " + indice);

   //ForEach
   users.forEach(function(user, index){
     if (user.id == req.params.id){
         indice = index;
     }
   });
   console.log("For each: " + indice);

   //For findIndex
   function existeUser(item, index, array){
     return item.id == req.params.id;
   }

   indice = users.findIndex(existeUser);

   console.log("For findIndex: " + indice);

   if (indice>-1){
      users.splice(indice, 1);
      io.writeUserDataFile(users);
   }

   var msg = (indice > -1) ? "Usuario borrado con éxito" : "Usuario no encontrado";
   res.send({"msg" : msg});
}

function deleteUserByIdV2(req,res){
  console.log("DELETE /apptechu/v2/users/:_id");

  var q = "_id:" + req.params.id + "?";


  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  httpClient.delete("user/" + q + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = {"msg" : "Error obteniendo el usuario"};
        res.status(500);
      }else{
        if (body.length > 0){
          var response = body[0];
        }else{
          var response = {"msg" : "Usuario no encontrado"};
          res.status(404);
        }
      }
      res.send(response);
    });
}

//Update
function updateUserByIdV1(req,res){
    console.log("PUT /apptechu/v1/users/:id");

    var users = require('../usuarios.json');
    var indice = -1;


    //for in
    for (index in users){
      if (users[index].id == req.params.id){
          indice = index;
          break;
      }
    }

    if(indice>-1){
      users[indice].first_name = req.body.first_name
      users[indice].last_name = req.body.last_name
      users[indice].email = req.body.email
      io.writeUserDataFile(users);
      res.send({"msg" : "Usuario actualizado con éxito"});
    }else {
      res.send({"msg" : "Usuario no encontrado"});
    }
}

module.exports.getUserV1 = getUserV1; //para que se pueda utilizar desde fuera
module.exports.getUserV2 = getUserV2;

module.exports.deleteUserByIdV1 = deleteUserByIdV1;
module.exports.deleteUserByIdV2 = deleteUserByIdV2;
module.exports.getUserByIdV1 = getUserByIdV1;
module.exports.getUserByIdV2 = getUserByIdV2;

module.exports.setUserV1 = setUserV1;
module.exports.setUserV2 = setUserV2;

module.exports.updateUserByIdV1 = updateUserByIdV1;
