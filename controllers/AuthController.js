const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujllr10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function login(req, res){

  var users = require('../usuarios.json');
  var indice = -1;


  //for in
  for (index in users){
    if (users[index].email == req.body.email && users[index].password == req.body.password){
        indice = index;
        break;
    }
  }

  if(indice>-1){
    users[indice].logged = true;
    io.writeUserDataFile(users);
    res.send({"msg" : "Usuario logado con éxito", "id" : users[indice].id});
  }else {
    res.send({"msg" : "Usuario no encontrado"});
  }
}

function loginV2(req, res){
  console.log('/apitechu/v2/login');

  var query = 'q={"email": "' + req.body.email + '"}';
  var httpClient = requestJson.createClient(baseMLabURL);
  //console.log("Client created");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = {"msg" : "Error obteniendo el email"};
        res.status(500);
      }else{
        if (body.length > 0){
          if(crypt.checkPassword(req.body.password, body[0].password)){
            var putBody = '{"$set":{"logged":true}}';
            console.log("user?" + query + "&" + mLabAPIKey);
            httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPUT, resMLabPUT, bodyPUT){
                  console.log("bodyPUT:")
                  console.log(bodyPUT);
                  if(errPUT){
                    var responsePUT = {"msg" : "Error al actualizar el usuario"};
                    res.status(500);
                  }else{
                    if (bodyPUT.n > 0){
                      var responsePUT = {"msg" : "Usuario logado", "id" : body[0].id};
                    }else{
                      var responsePUT = {"msg" : "Usuario no encontrado"};
                      res.status(404);
                    }
                  }
                  res.send(responsePUT);
                });
            //var response = {"msg" : "Password CORRECTO"};
            //res.status(200);
          }else{
            var response = {"msg" : "Password incorrecto"};
            res.status(401);
            res.send(response);
          }

        }else{
          var response = {"msg" : "Email no encontrado"};
          res.status(404);
          res.send(response);
        }
      }
    });
}

function logout(req, res){

  var users = require('../usuarios.json');
  var indice = -1;

  //for in
  for (index in users){
    if (users[index].id == req.params.id && users[index].logged == true){
        indice = index;
        break;
    }
  }

  if(indice>-1){

    delete users[indice].logged;
    io.writeUserDataFile(users);
    res.send({"msg" : "Usuario des-logado con éxito", "id" : users[indice].id});
  }else {
    res.send({"msg" : "Usuario no existe o no está logado"});
  }
}

function logoutV2(req, res){
  console.log('/apitechu/v2/logout/:id');

  var query = 'q={"id": ' + req.params.id + '}';
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log(baseMLabURL + "user?" + query + "&" + mLabAPIKey);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = {"msg" : "Error de servidor"};
        res.status(500);
      }else{
        if (body.length > 0){
            var putBody = '{"$unset":{"logged":""}}';

            httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPUT, resMLabPUT, bodyPUT){
                  console.log("bodyPUT:")
                  console.log(bodyPUT);
                  if(errPUT){
                    var responsePUT = {"msg" : "Error al actualizar el usuario"};
                    res.status(500);
                  }else{
                    if (bodyPUT.n > 0){
                      var responsePUT = {"msg" : "Usuario des-logado"};
                    }else{
                      var responsePUT = {"msg" : "Usuario no encontrado"};
                    }
                  }
                  res.send(responsePUT);
                });
            //var response = {"msg" : "Password CORRECTO"};
            //res.status(200);
        }else{
          var response = {"msg" : "Usuario no encontrado"};
          res.status(404);
          res.send(response);
        }
      }
    });
}

module.exports.login = login;
module.exports.logout = logout;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
