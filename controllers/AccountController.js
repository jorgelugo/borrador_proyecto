const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujllr10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getAccountByUserIdV2(req,res){
  console.log("GET /apptechu/v2/accounts/:id");


  var q = "q={idUser:" + req.params.id + "}&";

  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("account?" + q + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = {"msg" : "Error obteniendo las cuentas del usuario"};
        res.status(500);
      }else{
        if (body.length > 0){
          var response = body;
        }else{
          var response = {"msg" : "Usuario no encontrado"};
          res.status(404);
        }
      }
      res.send(response);
    });
}

module.exports.getAccountByUserIdV2 = getAccountByUserIdV2;
