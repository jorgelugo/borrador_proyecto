const fs = require('fs');

function writeUserDataFile(data){
  var serialUsers = JSON.stringify(data);

  fs.writeFile('./usuarios.json', serialUsers, "utf8",
  function (err) {
      if (err){
        console.log(err);
      } else {
        console.log("Usuario persistido");
      }
  });
}
 module.exports.writeUserDataFile = writeUserDataFile;
