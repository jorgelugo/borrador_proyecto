//Inicializamos el framework de MVC
const express = require('express');
const app = express();
require('dotenv').config();

const port = process.env.PORT || 3000; //Inicializamos port con variable de entorno a no ser que no exista que le podríamos el 3000

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
}

app.use(express.json());//Obligamos que procese el body de la petición como json
app.use(enableCORS);

app.listen(port); //se inicia un servidor

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');


console.log("API escuchando en el puerto: " + port);

//Get all users
app.get('/apitechu/v1/users',userController.getUserV1);
app.get('/apitechu/v2/users',userController.getUserV2);
//Get user by ID
app.get('/apitechu/v1/users/:id',userController.getUserByIdV1);
app.get('/apitechu/v2/users/:id',userController.getUserByIdV2);
//Insert user
app.post('/apitechu/v1/users',userController.setUserV1);
app.post('/apitechu/v2/users',userController.setUserV2);

//Delete by ID
app.delete('/apitechu/v1/users/:id',userController.deleteUserByIdV1);
app.delete('/apitechu/v2/users/:id',userController.deleteUserByIdV2);
//Update
app.put('/apitechu/v1/users/:id', userController.updateUserByIdV1);

//Autenticación
app.post('/apitechu/v1/login', authController.login);
app.post('/apitechu/v2/login', authController.loginV2);

app.post('/apitechu/v1/logout/:id', authController.logout);
app.post('/apitechu/v2/logout/:id', authController.logoutV2);

//Accounts by user id
app.get('/apitechu/v2/accounts/:id', accountController.getAccountByUserIdV2);


app.post("/apitechu/v1/mosntruo/:p1/:p2",
  function(req, res){
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query string");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
)
